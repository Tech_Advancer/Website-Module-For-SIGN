<?php
/////////////////////////////
//
//  delete.php
//  Can be included by any script
//  Deletes Comments
//  Expects $_GET['p'] to be
//    the comment number or
//    $commentNumber set by
//    another script.
/////////////////////////////

if((!isset($auto)) || ($auto=FALSE)){ //if another script is calling this
	if((isset($_GET['p'])) && (is_numeric($_GET['p']))){ //if commentNumber isset
		$commentNumber = trim($_GET['p']);
	}else{
		die("Error!");
	}
}//end if another script is calling this

if($userRank>0){ //if we are an admin or mod
	//Delete the Comment:
	//change database variables since other scripts call this
	$deleteLink = db_connect($database_url, $database_username, $database_password, $database_name);
	$deleteQuery = 'DELETE FROM website_' . $moduleNumber . '_comments WHERE number=' . $commentNumber;
	$deleteQuery = mysqli_real_escape_string($deleteLink, $deleteQuery);
	if(mysqli_query($deleteLink, $deleteQuery)){
		unset($deleteQuery);
		if((!isset($auto)) || ($auto=FALSE)){ //if another script is calling this, hide the confirm message
			$message = 'Comment Deleted.<br><a href="./index.php?m=' . $moduleNumber . '">Return</a>';
			include($root . $modulePath . $themePath . "header.html");
			include($root . $modulePath . $themePath . "message.html");
			include($root . $modulePath . $themePath . "footer.html");
		}
	mysqli_close($deleteLink);
	}else{
	        die("Error ");
	}

}//end if we are an admin or mod

?>
