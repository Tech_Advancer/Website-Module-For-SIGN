<?php
////////////////////////
//
//  admin_links.php
//  Included by module.php
//  Edit links for the
//  website module.
////////////////////////


if(((isset($_SESSION['website_' . $moduleNumber . '_admin_1'])) && (isset($_SESSION['website_' . $moduleNumber . '_admin_2'])) && ($_SESSION['website_' . $moduleNumber . '_admin_1']==$userRank) && ($_SESSION['website_' . $moduleNumber . '_admin_2']==$userRank))  && (($userRank==2) || ($userRank==3))){  //if you are an admin of the website

$link = db_connect($database_url, $database_username, $database_password, $database_name);

if(isset($_POST['pageLinks'])){
//if form was submitted
        $pageLinks = $_POST['pageLinks'];
        $query = 'UPDATE website_' . $moduleNumber . '_settings SET textValue=? WHERE name="links"';
        $stmt = mysqli_stmt_init($link);
        if(mysqli_stmt_prepare($stmt, $query)){
                mysqli_stmt_bind_param($stmt, "s", $pageLinks);
                mysqli_stmt_execute($stmt);
                mysqli_stmt_close($stmt);
                unset($query);
        }else{ //if stmt_prepare fails:
                die("Error!");
        }
} //end if form has been submitted

//Get current links below:
$query = 'SELECT textValue FROM website_' . $moduleNumber . '_settings WHERE name="links"';
if($result = mysqli_query($link, $query)){
        while($row = mysqli_fetch_object($result)){
		$defaultValue = $row->textValue;
        }
}
unset($query); unset($row); unset($result);
mysqli_close($link);




include('.' . $modulePath . 'admin_header.php');
if(isset($_POST['pageLinks'])){
//if form was submitted
?>
<h1>Links Updated!</h1>
<p><a href="./index.php?m=<?php echo $moduleNumber; ?>">Click here to view</a> your changes!</p>
<?php } ?>

<h1>Update links for <?php echo $moduleName; ?></h1>

<form action="./index.php?m=<?php echo $moduleNumber; ?>&a=9" method="POST">
<textarea rows="15" cols="50" name="pageLinks"><?php if(isset($defaultValue)){ echo $defaultValue; } ?></textarea><br><br>
<input type="submit" value="Update">
</form>

<?php
include('.' . $modulePath . 'admin_footer.php');
} //if you are an admin of the website

?>
