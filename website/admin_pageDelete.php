<?php
////////////////////////
//
//  admin_pageDelete.php
//  Included by module.php
//  Admins only.
//  Deletes pages.
////////////////////////


if(((isset($_SESSION['website_' . $moduleNumber . '_admin_1'])) && (isset($_SESSION['website_' . $moduleNumber . '_admin_2'])) && ($_SESSION['website_' . $moduleNumber . '_admin_1']==$userRank) && ($_SESSION['website_' . $moduleNumber . '_admin_2']==$userRank))  && (($userRank==2) || ($userRank==3))){  //if you are an admin of the website

$link = db_connect($database_url, $database_username, $database_password, $database_name);  //keep this open!

if((isset($_GET['p'])) && (is_numeric($_GET['p']))){ $pageNumber = $_GET['p']; }else{ session_destroy(); die("Error!"); }

//Query the homePage to be safe
$query = 'SELECT intValue FROM website_' . $moduleNumber . '_settings WHERE name="homePage"';
if($result = mysqli_query($link, $query)){
        while($row = mysqli_fetch_object($result)){
                if($pageNumber == $row->intValue){
                        //if so die with an error
                        die("<h1>You cannot delete a home page!</h1><br><h3>Change the home page in the page list!</h3>");
                }
        }
}
unset($query); unset($row); unset($result);

//Query this page to get name to double check that user clicked on the right thing
$query = 'SELECT name,date,author FROM website_' . $moduleNumber . '_pages WHERE number=' . $pageNumber;
if($result = mysqli_query($link, $query)){
        while($row = mysqli_fetch_object($result)){
                $pageName = $row->name;
		$pageDate = $row->date;
		$pageAuthor = $row->author;
        }
}
unset($query); unset($row); unset($result);


if((isset($_GET['v'])) && ($_GET['v']==1)){
	//if we have confirmed to delete the page

	//delete plinks
        $query = 'DELETE FROM website_' . $moduleNumber . '_plink WHERE pageNumber=' . $pageNumber;
        if(!$result = mysqli_query($link, $query)){
                die("Error!");
        }
        unset($query); unset($result);

	//delete the page
	$query = 'DELETE FROM website_' . $moduleNumber . '_pages WHERE number=' . $pageNumber;
	if(!$result = mysqli_query($link, $query)){
        	die("Error!");
	}
	unset($query); unset($result);

include('.' . $modulePath . 'admin_header.php');
?>
<h1>Deleted <?php echo $pageName; ?>!</h1>
<?php
include('.' . $modulePath . 'admin_footer.php');
}

mysqli_close($link);

if((!isset($_GET['v'])) || ($_GET['v']!=1)){ //if not confirmed
include('.' . $modulePath . 'admin_header.php');
?>
<h1>Are you sure you want to delete page <?php echo "(" . $pageNumber . ") " . $pageName; ?> create by <?php echo $pageAuthor; ?> on <?php echo $pageDate; ?>?</h1><h2>You cannot undo this!</h2>
<a href="./index.php?m=<?php echo $moduleNumber; ?>&a=11&p=<?php echo $pageNumber; ?>&v=1">Yes, Continue.</a>
<?php
include('.' . $modulePath . 'admin_footer.php');
} //end if not confirmed
} //if you are an admin of the website
?>
