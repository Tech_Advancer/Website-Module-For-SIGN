<?php
////////////////////////
//
//  admin_postList.php
//  Included by module.php
//  Admin Only
//  Lists all posts on module.
////////////////////////


if(((isset($_SESSION['website_' . $moduleNumber . '_admin_1'])) && (isset($_SESSION['website_' . $moduleNumber . '_admin_2'])) && ($_SESSION['website_' . $moduleNumber . '_admin_1']==$userRank) && ($_SESSION['website_' . $moduleNumber . '_admin_2']==$userRank))  && (($userRank==2) || ($userRank==3))){  //if you are an admin of the website

include('.' . $modulePath . 'admin_header.php');
?>

<h1>Post List</h1>

<table style="width: 95%; margin: auto; border-collapse: collapse;">
<?php
$link = db_connect($database_url, $database_username, $database_password, $database_name);

//Get all of the posts:
$query = 'SELECT number,name FROM website_' . $moduleNumber . '_posts';
$query = mysqli_real_escape_string($link, $query);
?>
<tr style="background-color: rgb(181,181,181);"><th width="70%;">Post</th><th width="30%;">Settings</th></tr>
<?php
if($result = mysqli_query($link, $query)){
	$i=0;
	while($row = mysqli_fetch_object($result)){
	$i = $i + 1;
?>
<tr><td>
                <a href="./index.php?m=<?php echo $moduleNumber; ?>&post=<?php echo $row->number; ?>">(<?php echo $row->number . ") &nbsp;" . $row->name; ?></a>
</td><td>
		<a href="./index.php?m=<?php echo $moduleNumber; ?>&a=4&p=<?php echo $row->number; ?>">Edit</a>&nbsp;|&nbsp;
		<a href="./index.php?m=<?php echo $moduleNumber; ?>&a=10&p=<?php echo $row->number; ?>">Delete</a>
</td></tr>
<?php
	}
}
unset($query); unset($row); unset($result);
mysqli_close($link);
?>
</table>
<?php
include('.' . $modulePath . 'admin_footer.php');
} //if you are an admin of the website

?>
