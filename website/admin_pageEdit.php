<?php
////////////////////////
//
//  admin_settings.php
//  Included by module.php
//
////////////////////////


if(((isset($_SESSION['website_' . $moduleNumber . '_admin_1'])) && (isset($_SESSION['website_' . $moduleNumber . '_admin_2'])) && ($_SESSION['website_' . $moduleNumber . '_admin_1']==$userRank) && ($_SESSION['website_' . $moduleNumber . '_admin_2']==$userRank))  && (($userRank==2) || ($userRank==3))){  //if you are an admin of the website


if((isset($_GET['p'])) && (is_numeric($_GET['p']))){
	$pageNumber = $_GET['p'];
}else{
	if((isset($_POST['pageNumber'])) && (is_numeric($_POST['pageNumber']))){
		$pageNumber = $_POST['pageNumber'];
	}else{
		die("Error!1");
	}
}

$link = db_connect($database_url, $database_username, $database_password, $database_name);
$updated =false;

if((isset($_POST['pageName'])) && (isset($_POST['pageContent'])) && (isset($_POST['pageType'])) && (isset($_POST['pageDate'])) && (isset($_POST['pageAuthor']))){

	$pageName = $_POST['pageName'];
	$pageContent = $_POST['pageContent'];
	$pageType = $_POST['pageType'];
	$pageDate = $_POST['pageDate'];
	$pageAuthor = $_POST['pageAuthor'];

	$query = 'UPDATE website_' . $moduleNumber . '_pages SET name=?,content=?,type=?,date=?,author=? WHERE number=' . $pageNumber;
        $stmt = mysqli_stmt_init($link);
        if(mysqli_stmt_prepare($stmt, $query)){
                mysqli_stmt_bind_param($stmt, "ssiss", $pageName, $pageContent, $pageType, $pageDate, $pageAuthor);
                mysqli_stmt_execute($stmt);
                mysqli_stmt_close($stmt);
                unset($query);
        }else{ //if stmt_prepare fails:
                die("Error!");
        }
	$updated = true;
}

//Get page info:
$query = 'SELECT name,content,type,date,author FROM website_' . $moduleNumber . '_pages WHERE number=' . $pageNumber;
if($result = mysqli_query($link, $query)){
	while($row = mysqli_fetch_object($result)){
                $pageName = $row->name;
		$pageContent = $row->content;
		$pageType = $row->type;
		$pageDate = $row->date;
		$pageAuthor = $row->author;
        }
}else{
	die("Error!<br>" . $query);
}
unset($query); unset($row); unset($result);
mysqli_close($link);

include('.' . $modulePath . 'admin_header.php');
?>

<h1>Edit (<?php echo $pageNumber . ") " . $pageName; ?></h1>

<?php
if($updated){ //if updated
?>
<h3>Updated!</h3>
<?php
} //end if updated
?>
<form action="./index.php?m=<?php echo $moduleNumber; ?>&a=7" method="POST">
<input type="hidden" name="pageNumber" value="<?php echo $pageNumber; ?>">
<label>Name: <input type="text" name="pageName" value="<?php echo $pageName; ?>"></label><br><br>
<label>Type: <select name="pageType">
<option value="<?php echo $pageType; ?>">
<?php
switch($pageType){
case 1: echo "Normal Page"; break;
case 2: echo "List Page"; break;
case 3: echo "Static Page"; break;
case 4: echo "Blank Static Page"; break;
default: die("</option></select>Error!");
}
?>
</option>
<option value="1">Normal Page</option>
<option value="2">List Page</option>
<option value="3">Static Page</option>
<option value="4">Blank Static Page</option>
</select></label><br><br>
<label>Author: <input type="text" name="pageAuthor" value="<?php echo $pageAuthor; ?>"></label><br><br>
<label>Date: <input type="text" name="pageDate" value="<?php echo $pageDate; ?>"></label><br><br>
<label>Content:<br>
<textarea rows="15" cols="50" name="pageContent"><?php echo $pageContent; ?></textarea></label><br><br><br>
<input type="submit" value="Edit">
</form>

<br><br>
<a href="./index.php?m=<?php echo $moduleNumber; ?>&page=<?php echo $pageNumber; ?>">See Page ></a>
<br><br>

<?php
include('.' . $modulePath . 'admin_footer.php');
} //if you are an admin of the website
?>
