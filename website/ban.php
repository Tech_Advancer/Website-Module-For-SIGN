<?php
/////////////////////////////
//
//  ban.php
//  Included by module.php
//  Loads page to ban users.
/////////////////////////////

if((isset($_GET['p'])) && (is_numeric($_GET['p']))){ //if user to ban's number isset
        $banUser = trim($_GET['p']);
}else{
        die("Error!");
}

if(!isset($_POST['submit'])){ //if we are not banning the user, yet

if(($userRank>0) && ($banUser!=$_SESSION['user_number'])){ //if we are an admin and aren't banning ourselves
	//Get Info on User to be banned:
	$link = db_connect($database_url, $database_username, $database_password, $database_name);
	$query = 'SELECT username,joinDate,lastLogin,emailAddress,modNotes,banReason,ipList,banYear,banMonth,banDay FROM shared_users WHERE number=' . $banUser;

	if($resultLink = mysqli_query($link, $query)){
		$result = mysqli_fetch_assoc($resultLink);
		$banUserName = $result['username'];
		$banUserJoinDate = $result['joinDate'];
		$banUserLastLogin = $result['lastLogin'];
		$banUserEmail = $result['emailAddress'];
		$banUserModNotes = $result['modNotes'];
		$banUserBanReason = $result['banReason'];
		$banUserIp = $result['ipList'];
		$banUserYear = $result['banYear'];
		$banUserMonth = $result['banMonth'];
		$banUserDay = $result['banDay'];
	}

	mysqli_free_result($resultLink);
	unset($query); unset($result);
	//End Get Info on User to be banned

	$query = 'SELECT rank FROM website_' . $moduleNumber . '_users WHERE number=' . $banUser;

        if($resultLink = mysqli_query($link, $query)){
                $result = mysqli_fetch_assoc($resultLink);
                $banUserRank = $result['rank'];
        }

        mysqli_close($link);
        unset($query); unset($result);
        //End Get Info on User to be banned

        include($root . $modulePath . $themePath . "header.html");
        if(($userRank==1) && ($banUserRank>0)){ //if a mod is trying to ban another mod or admin
                die("<h2>You can't ban an admin or another mod! Ask an admin.</h2>");
        }else{
                include($root . $modulePath . $themePath . "ban.html");
        }
        include($root . $modulePath . $themePath . "footer.html");

}//end if we are an admin and not banning ourselves

}else{ //ban the user!

//double check ban user's rank
$link = db_connect($database_url, $database_username, $database_password, $database_name);
$query = 'SELECT rank FROM website_' . $moduleNumber . '_users WHERE number=' . $banUser;
if($resultLink = mysqli_query($link, $query)){
        $result = mysqli_fetch_assoc($resultLink);
        $banUserRank = $result['rank'];
}
unset($query); unset($result);

if(isset($_POST['modNotes'])){ $modNotes = $_POST['modNotes']; }else{ $modNotes = NULL; }
if((isset($_POST['banMonth'])) && (strlen(trim($_POST['banMonth']))>0) && (is_numeric(trim($_POST['banMonth'])))){ $banMonth = (int)trim($_POST['banMonth']); }else{ $banMonth = NULL; }
if((isset($_POST['banDay'])) && (strlen(trim($_POST['banDay']))>0) && (is_numeric(trim($_POST['banDay'])))){ $banDay = (int)trim($_POST['banDay']); }else{ $banDay = NULL; }
if((isset($_POST['banYear'])) && (strlen(trim($_POST['banYear']))>0) && (is_numeric(trim($_POST['banYear'])))){ $banYear = (int)trim($_POST['banYear']); }else{ $banYear = NULL; }
if(isset($_POST['banReason'])){ $banReason = $_POST['banReason']; }else{ $banReason = NULL; }
if(isset($_POST['deleteAll'])){ $deleteAll = TRUE; }else{ $deleteAll = FALSE; }

$query = 'UPDATE shared_users SET modNotes=?,banReason=?,banYear=?,banMonth=?,banDay=? WHERE number=' . $banUser;
$query = mysqli_real_escape_string($link, $query);
$stmt = mysqli_stmt_init($link);

if(mysqli_stmt_prepare($stmt, $query)){
	mysqli_stmt_bind_param($stmt, "ssiii", $modNotes, $banReason, $banYear, $banMonth, $banDay);
	mysqli_stmt_execute($stmt);
	mysqli_stmt_close($stmt);
}else{
	die("Error!" . mysqli_error($link));
}
unset($query); unset($stmt);

if((is_null($banYear)) && (is_null($banMonth)) && (is_null($banDay))){ //if the user isn't being banned, don't delete comments
$deleteAll = FALSE;
}

if($deleteAll){
	//delete all comments:
     	$query = 'SELECT number FROM website_' . $moduleNumber . '_comments WHERE author=' . $banUser;
      	$query = mysqli_real_escape_string($link, $query);
     	if($result = mysqli_query($link, $query)){
               	while($row = mysqli_fetch_object($result)){
			$auto = TRUE;
			$commentNumber = $row->number;
			include('delete.php');
		}
      	}else{
          	die("Error ");
   	}
 	mysqli_free_result($result); unset($query); unset($row); unset($result);
}


if((!is_null($banYear)) && (!is_null($banMonth)) && (!is_null($banDay))){
$message = 'User banned!<br><a href="./index.php?m=' . $moduleNumber . '&e=5&p=' . $banUser . '">Return to Ban Page</a><br><a href="./index.php?m=' . $moduleNumber . '">Return To Module Front Page</a>';
}else{
$message = 'User updated, but not banned.<br><a href="./index.php?m=' . $moduleNumber . '&e=5&p=' . $banUser . '">Return to Ban Page</a><br><a href="./index.php?m=' . $moduleNumber . '">Return To Module Front Page</a>';
}
include($root . $modulePath . $themePath . "header.html");
include($root . $modulePath . $themePath . "message.html");
include($root . $modulePath . $themePath . "footer.html");


} //end ban the user!
?>
