<?php
////////////////////////////
//
//  moduleJoin.php
//  Included by an external join.php
//  Used to set db values for this module
//    when joining from another module.
//  Expects $userNum and $subModuleNumber
//    to be set beforehand!
///////////////////////////

//All variables named sub(whatever) since this is run under the main installer

$subQuery = 'INSERT INTO website_' . $subModuleNumber . '_users (number, commentNumber) VALUES (?, 0)';

$subStmt = mysqli_stmt_init($link);

if(mysqli_stmt_prepare($subStmt, $subQuery)){
	mysqli_stmt_bind_param($subStmt, "i" , $userNum);
	mysqli_stmt_execute($subStmt);
	mysqli_stmt_close($subStmt);
	unset($subStmt); unset($subQuery);
}else{
die("Error! Join failed!");
}

?>
