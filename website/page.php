<?php
//////////////////////////
//
//  page.php
//  Included by module.php
//    Loads the posts and
//    displays the pages.
//  Requires $pageNumber!
//////////////////////////

//Get page information from website_(module#)_pages below:
$link = db_connect($database_url, $database_username, $database_password, $database_name);
$query = 'SELECT name,content,type,date,author FROM website_' . $moduleNumber . '_pages WHERE number=' . $pageNumber;
$query = mysqli_real_escape_string($link, $query);

if($resultLink = mysqli_query($link, $query)){
$result = mysqli_fetch_assoc($resultLink);
$pageName = $result['name'];
$pageContent = $result['content'];
$pageType = $result['type'];
$pageDate = $result['date'];
$pageAuthor = $result['author'];

mysqli_free_result($resultLink);
unset($result); unset($resultLink);
}else{
die("Error!");
}
unset($query);
//Get page information from website_(module#)_pages above

//Load Posts into array below:
if(($pageType==1) || ($pageType==2)){//if pageType is normal or list
	//Sub-Query below for faster grab of posts:
	$query = 'SELECT number,name,content,date,author,comments FROM website_' . $moduleNumber . '_posts WHERE number IN (SELECT postNumber FROM website_' . $moduleNumber . '_plink WHERE pageNumber=' . $pageNumber . ')';
	$query = mysqli_real_escape_string($link, $query);
	$postCount = 0;

	if($result = mysqli_query($link, $query)){
		while($row = mysqli_fetch_object($result)){
			$postNumber[$postCount] = $row->number;
			$postName[$postCount] = $row->name;
			$postDate[$postCount] = $row->date;
			$postAuthor[$postCount] = $row->author;
			$postContent[$postCount] = $row->content;
			$postComments[$postCount] = $row->comments;
			$postCount = $postCount + 1;
		}
	}
}//end if pageType is normal or list
//include theme files
switch($pageType){
	case 1: include($root . $modulePath . $themePath . "header.html");
		include($root . $modulePath . $themePath . "normalPage.html");
		include($root . $modulePath . $themePath . "footer.html");
		break;
	case 2: include($root . $modulePath . $themePath . "header.html");
		include($root . $modulePath . $themePath . "listPage.html");
		include($root . $modulePath . $themePath . "footer.html");
		break;
	case 3: include($root . $modulePath . $themePath . "header.html");
		include($root . $modulePath . $themePath . "staticPage.html");
		include($root . $modulePath . $themePath . "footer.html");
		break;
	case 4: echo $pageContent; break;
	default: include($root . $modulePath . $themePath . "header.html");
		include($root . $modulePath . $themePath . "missingPage.html");
		include($root . $modulePath . $themePath . "footer.html");
		break;
}
?>
