<?php
////////////////////////
//
//  admin_postCreate.php
//  Included by module.php
//  Makes posts for the
//  website module.
////////////////////////


if(((isset($_SESSION['website_' . $moduleNumber . '_admin_1'])) && (isset($_SESSION['website_' . $moduleNumber . '_admin_2'])) && ($_SESSION['website_' . $moduleNumber . '_admin_1']==$userRank) && ($_SESSION['website_' . $moduleNumber . '_admin_2']==$userRank))  && (($userRank==2) || ($userRank==3))){  //if you are an admin of the website

$link = db_connect($database_url, $database_username, $database_password, $database_name);  //keep this open!

if((isset($_POST['postName'])) && (isset($_POST['postContent']))){
//if form was submitted

	$i=0;
	$pages=$_POST['pages'];
	foreach($pages as $pagez=>$value){
		$i = $i + 1;
	}
	if($i==0){ die("Error! No pages selected!"); }

        $postName = $_POST['postName'];
        $postContent = $_POST['postContent'];
	$postAuthor = $_SESSION['user_name'];
	$postDate = date("Y-m-d");
	if(isset($_POST['postComments'])){ $postComments=1; }else{ $postComments=0; }

        $query = 'INSERT INTO website_' . $moduleNumber . '_posts (name,content,date,author,comments) VALUES (?,?,?,?,?)';
        $stmt = mysqli_stmt_init($link);
        if(mysqli_stmt_prepare($stmt, $query)){
                mysqli_stmt_bind_param($stmt, "ssssi", $postName, $postContent, $postDate, $postAuthor, $postComments);
                mysqli_stmt_execute($stmt);
                $postNumber = mysqli_insert_id($link);
                mysqli_stmt_close($stmt);
                unset($query);
        }else{ //if stmt_prepare fails:
                die("Error!");
        }

	foreach($pages as $page=>$values){
		$query = 'INSERT INTO website_' . $moduleNumber . '_plink (pageNumber,postNumber) VALUES(' . $values . ',' . $postNumber . ')';
		if(!$result = mysqli_query($link, $query)){
		        die("Error!");
		}
		unset($query); unset($result);
	}

	include('.' . $modulePath . 'admin_header.php');
?>
<h1>Post Created!</h1>
<p><a href="./index.php?m=<?php echo $moduleNumber; ?>&a=4&p=<?php echo $postNumber; ?>">Click Here to edit</a> or
<a href="./index.php?m=<?php echo $moduleNumber; ?>&post=<?php echo $postNumber; ?>">click here to view</a> your new post!</p>
<?php
	include('.' . $modulePath . 'admin_footer.php');
}else{
//if form hasn't been submitted
	include('.' . $modulePath . 'admin_header.php');
?>

<h1>Create a New Post for <?php echo $moduleName; ?></h1>

<form action="./index.php?m=<?php echo $moduleNumber; ?>&a=5" method="POST">
<label>Post Name: <input type="text" name="postName"></label><br><br>
<label>Check to turn on Comments: <input type="checkbox" name="postComments" value="1"></label><br><br>
Add to page:
<div style="width: 200px; height: 100px; margin: auto; overflow-y: scroll; border: solid 2px black; text-align: left;">
<?php

	//Get all pages:
	$query = 'SELECT number,name FROM website_' . $moduleNumber . '_pages';
	$query = mysqli_real_escape_string($link, $query);
	if($result = mysqli_query($link, $query)){
	        while($row = mysqli_fetch_object($result)){
?>
<label>&nbsp;&nbsp;<input type="checkbox" name="pages[]" value="<?php echo $row->number; ?>"> <?php echo $row->name; ?></label><br>
<?php
		}
	}
	unset($query); unset($row); unset($result);
	mysqli_close($link);
?>
</div>

<br><br>
<label>Post:<br>
<textarea rows="15" cols="50" name="postContent"></textarea></label><br><br>
<input type="submit" value="Create">
</form>

<?php
	include('.' . $modulePath . 'admin_footer.php');
} // end if the form has been submitted
} //if you are an admin of the website

?>
