<?php
////////////////////////////////
//
// installation_variables.php
//
// Define important variables specific to this installation here.
// Included by module.php
////////////////////////////////


//Get User's Rank if Logged in:
if((isset($_SESSION['user_number'])) && ($_SESSION['user_number']>=0)){
	$link = db_connect($database_url, $database_username, $database_password, $database_name);
	$query = 'SELECT rank FROM website_' . $moduleNumber . '_users WHERE number=' . $_SESSION['user_number'];

	if($resultLink = mysqli_query($link, $query)){
		$result = mysqli_fetch_assoc($resultLink);
		$userRank=$result['rank']; //Should NOT be a session variable; We don't want this to follow users around and cause an error!
	}

	mysqli_free_result($resultLink);
	mysqli_close($link);
	unset($query); unset($result); unset($link);
}else{ //End Get User's Rank if Logged in
	$userRank = -1;
}

?>
