<?php
////////////////////////
//
//  admin_userEditor.php
//  Included by module.php
//
////////////////////////


if(((isset($_SESSION['website_' . $moduleNumber . '_admin_1'])) && (isset($_SESSION['website_' . $moduleNumber . '_admin_2'])) && ($_SESSION['website_' . $moduleNumber . '_admin_1']==$userRank) && ($_SESSION['website_' . $moduleNumber . '_admin_2']==$userRank))  && (($userRank==2) || ($userRank==3))){  //if you are an admin of the website


if((isset($_GET['p'])) && (is_numeric($_GET['p']))){
	$userNumber = $_GET['p'];
}else{
	if((isset($_POST['userNumber'])) && (is_numeric($_POST['userNumber']))){
		$userNumber = $_POST['userNumber'];
	}else{
		die("Error!1");
	}
}

$link = db_connect($database_url, $database_username, $database_password, $database_name);
$updated =false;

if((isset($_POST['userNumber'])) && (isset($_POST['useraRank']))){

	$useraRank = $_POST['useraRank'];

	$query = 'UPDATE website_' . $moduleNumber . '_users SET rank=? WHERE number=' . $userNumber;
        $stmt = mysqli_stmt_init($link);
        if(mysqli_stmt_prepare($stmt, $query)){
                mysqli_stmt_bind_param($stmt, "i", $useraRank);
                mysqli_stmt_execute($stmt);
                mysqli_stmt_close($stmt);
                unset($query);
        }else{ //if stmt_prepare fails:
                die("Error!");
        }
	$updated = true;
}

//Get user info:
$query = 'SELECT username FROM shared_users WHERE number=' . $userNumber;
if($result = mysqli_query($link, $query)){
	while($row = mysqli_fetch_object($result)){
                $userName = $row->username;
        }
}else{
	die("Error!");
}
unset($query); unset($row); unset($result);

$query = 'SELECT rank FROM website_' . $moduleNumber . '_users WHERE number=' . $userNumber;
if($result = mysqli_query($link, $query)){
        while($row = mysqli_fetch_object($result)){
                $useraRank = $row->rank;
        }
}else{
        die("Error!");
}
unset($query); unset($row); unset($result);

mysqli_close($link);

if(($useraRank==3) && ($userRank<3)){
	include('.' . $modulePath . 'admin_header.php');
        echo "<h1>This user's rank is too high for you to change!</h1>";
        include('.' . $modulePath . 'admin_footer.php');
        die();
}

if($_SESSION['user_number']==$userNumber){
	include('.' . $modulePath . 'admin_header.php');
	echo "<h1>You can't edit yourself silly!</h1>";
	include('.' . $modulePath . 'admin_footer.php');
	die();
}

include('.' . $modulePath . 'admin_header.php');
?>

<h1>Edit (<?php echo $userNumber . ") " . $userName; ?>'s Rank on <?php echo $moduleName; ?></h1>

<?php
if($updated){ //if updated
?>
<h3>Updated!</h3>
<?php
} //end if updated
?>
<form action="./index.php?m=<?php echo $moduleNumber; ?>&a=12" method="POST">
<input type="hidden" name="userNumber" value="<?php echo $userNumber; ?>">
<?php
if($userRank==3){ //logged in user is overall admin
?>
<label>Type: <select name="useraRank">
<option value="<?php echo $useraRank; ?>"><?php
switch($useraRank){
case 0: echo "Normal User"; break;
case 1: echo "Moderator"; break;
case 2: echo "Administrator"; break;
case 3: echo "Overall Administrator"; break;
default: die("</option></select>Error!");
}
?>
</option>
<option value="0">Normal User</option>
<option value="1">Moderator</option>
<option value="2">Administrator</option>
<option value="3">Overall Administrator</option>
</select></label><br><br>
<?php
}else{ //logged in user is just a normal admin:
?>
<label>Type: <select name="useraRank">
<option value="<?php echo $useraRank; ?>"><?php
switch($useraRank){
case 0: echo "Normal User"; break;
case 1: echo "Moderator"; break;
case 2: echo "Administrator"; break;
default: die("</option></select>Error!");
}
?>
</option>
<option value="0">Normal User</option>
<option value="1">Moderator</option>
<option value="2">Administrator</option>
</select></label><br><br>
<?php
}
?>
<input type="submit" value="Update">
</form>

<?php
include('.' . $modulePath . 'admin_footer.php');
} //if you are an admin of the website
?>
