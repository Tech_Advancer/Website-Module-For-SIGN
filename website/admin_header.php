<?php
//////////////////////////////
//
// admin_header.php
//  For use in website module
//  admin panel.
/////////////////////////////

if(((isset($_SESSION['website_' . $moduleNumber . '_admin_1'])) && (isset($_SESSION['website_' . $moduleNumber . '_admin_2'])) && ($_SESSION['website_' . $moduleNumber . '_admin_1']==$userRank) && ($_SESSION['website_' . $moduleNumber . '_admin_2']==$userRank)) && (($userRank==2) || ($userRank==3))){
//check if user is admin
?>
<!DOCTYPE html>
<html lang="en">
<head>
<title><?php echo $moduleName; ?> Admin</title>
<style type="text/css">
body{
text-align: center;
}

tr,td,th{
margin: auto;
text-align: center;
border: solid 1px black;
}

a{
letter-spacing: 1px;
margin: 2px;
text-decoration: none;
}

a:hover{
letter-spacing: 0px;
margin: 2px;
text-decoration: underline;
}
</style>
</head>
<body>

<div style="margin: auto; float: left; width: 100%; border: solid 1px black;"> <!-- header -->

<div style="float: left; margin: 4px;">
<a href="./index.php?a=1">SIGN Admin</a>&nbsp;|&nbsp;
<a href="./index.php?m=<?php echo $moduleNumber; ?>&a=1"><?php echo $moduleName; ?> Admin</a>&nbsp;|&nbsp;
<a href="./index.php?m=<?php echo $moduleNumber; ?>&a=13">Edit Theme</a>&nbsp;|&nbsp;
<a href="./index.php?m=<?php echo $moduleNumber; ?>&a=9">Edit Links</a>
</div>

<div style="float: right; margin: 4px;">
<a href="./index.php?m=<?php echo $moduleNumber; ?>&a=2">Users</a> | <a href="./index.php?m=<?php echo $moduleNumber; ?>&a=0">Logout of Admin Panel</a>
</div>

</div> <!-- end header -->

<br>

<?php
}//end check if user is admin
?>
