<?php
//////////////////////////
//
//  post.php
//  Included by module.php
//    Loads a single post.
//////////////////////////

//////////////////////////////////////////////////////
//		NEED TO CHECK IF COMMENT IS SUBMITTED
/////////////////////////////////////////////////////
if((isset($_GET['post'])) && (is_numeric(trim($_GET['post'])))){
        $postNumber = trim($_GET['post']);
	$link = db_connect($database_url, $database_username, $database_password, $database_name);

	if((isset($_POST['comment'])) && (strlen(trim($_POST['comment']))>1)){ //if a comment is sent
		//insert into database
		$comment = trim($_POST['comment']);
		$query = 'Insert INTO website_' . $moduleNumber . '_comments (postNumber, content, author) VALUES (' . $postNumber . ',?,' . $_SESSION['user_number'] . ')';
		$query = mysqli_real_escape_string($link, $query);
		$stmt = mysqli_stmt_init($link);

		if(mysqli_stmt_prepare($stmt, $query)){
		        mysqli_stmt_bind_param($stmt, "s" , $comment);
		        mysqli_stmt_execute($stmt);
		        mysqli_stmt_close($stmt);
		        unset($stmt); unset($query); unset($comment);
		}else{
			die("Error!");
		}
	}// end if comment is sent


	//Get page information from website_(module#)_pages below:
	$query = 'SELECT name,content,date,author,comments FROM website_' . $moduleNumber . '_posts WHERE number=' . $postNumber;
	$query = mysqli_real_escape_string($link, $query);

	if($resultLink = mysqli_query($link, $query)){
		$result = mysqli_fetch_assoc($resultLink);
		$postName = $result['name'];
		$postContent = $result['content'];
		$postDate = $result['date'];
		$postAuthor = $result['author'];
		$postComments = $result['comments']; //boolean; if the post allows for comments;

		mysqli_free_result($resultLink);
		unset($result); unset($resultLink);
	}else{
		die("Error!");
	}
	unset($query);
	//Get page information from website_(module#)_pages above


	if($postComments){ //if comments are allowed, load them:
		$query = 'SELECT number,content,author FROM website_' . $moduleNumber . '_comments WHERE postNumber=' . $postNumber;
	        $query = mysqli_real_escape_string($link, $query);

	        if($resultLink = mysqli_query($link, $query)){
			$commentCount=0;
	                while($row = mysqli_fetch_object($resultLink)){
				$commentNumber[$commentCount] = $row->number;
				$commentContent[$commentCount] = $row->content; //make all of these an array for easy use in the theme
				$commentAuthorNumber[$commentCount] = $row->author;
					//get Author name from $commentAuthorNumber
		                        $subQuery = 'SELECT username FROM shared_users WHERE number=' . $commentAuthorNumber[$commentCount];
		                        $subQuery = mysqli_real_escape_string($link, $subQuery);
		                        if($subResult = mysqli_query($link, $subQuery)){
		                                $subRow = mysqli_fetch_assoc($subResult);
	                                	$commentAuthor[$commentCount] = $subRow['username'];
	                       		 }else{
	                	                die("Error ");
	        	                }
		                        mysqli_free_result($subResult); unset($subQuery); unset($subRow); unset($subResult);
				$commentCount = $commentCount + 1;
			}
	                mysqli_free_result($resultLink);
	                unset($result); unset($resultLink);
	        }else{
	                die("Error!");
	        }
	        unset($query);
	} //end if comments are allowed

	unset($link);
	include($root . $modulePath . $themePath . "header.html");
	include($root . $modulePath . $themePath . "post.html");
	include($root . $modulePath . $themePath . "footer.html");

}else{ //if no post number set in GET data
	include($root . $modulePath . $themePath . "header.html");
	include($root . $modulePath . $themePath . "missingPage.html");
	include($root . $modulePath . $themePath . "footer.html");
}

?>
