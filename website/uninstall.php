<?php
//////////////////////
//
//  uninstall.php
//  Included in admin panel.
//  Uninstalls a website
//   module.
//  Expects GET value p.
//////////////////////

if((isset($admin_load_check)) && ($admin_load_check==$_SESSION['user_number']) && (isset($_SESSION['is_admin_1'])) && (isset($_SESSION['is_admin_2']))){
//check if user is an admin
if((isset($_GET['p'])) && (is_numeric(trim($_GET['p'])))){
//if a module number is set

$link = db_connect($database_url, $database_username, $database_password, $database_name);  //keep this open!

//Check if this module is set to load first
$query = 'SELECT intValue FROM shared_settings WHERE name="loadFirst"';
if($result = mysqli_query($link, $query)){
	while($row = mysqli_fetch_object($result)){
		$dbLoadFirst = $row->intValue;
		if($moduleNumber == $row->intValue){
			//if so die with an error
			die("<h1>You cannot delete a module set to load first!</h1><br><h3>Change which module loads first in the module list.</h3>");
		}
	}
}
unset($query); unset($row); unset($result);

//Remove this module from user's loadFirst settings
$query = 'UPDATE shared_users SET loadFirst=' . $dbLoadFirst . ' WHERE loadFirst=' . $moduleNumber;
if(!$result = mysqli_query($link, $query)){
        die("Error!");
}
unset($query); unset($row); unset($result);

// DROP STATEMENTS BELOW:

$query = 'DELETE FROM shared_installations WHERE number=' . $moduleNumber;
if(!$result = mysqli_query($link, $query)){
        die("Error!");
}
unset($query); unset($row); unset($result);

$query = 'DROP TABLE website_' . $moduleNumber . '_users';
if(!$result = mysqli_query($link, $query)){
	die("Error!");
}
unset($query); unset($row); unset($result);

$query = 'DROP TABLE website_' . $moduleNumber . '_settings';
if(!$result = mysqli_query($link, $query)){
        die("Error!");
}
unset($query); unset($row); unset($result);

$query = 'DROP TABLE website_' . $moduleNumber . '_plink';
if(!$result = mysqli_query($link, $query)){
        die("Error!");
}
unset($query); unset($row); unset($result);

$query = 'DROP TABLE website_' . $moduleNumber . '_comments';
if(!$result = mysqli_query($link, $query)){
        die("Error!");
}
unset($query); unset($row); unset($result);

$query = 'DROP TABLE website_' . $moduleNumber . '_posts';
if(!$result = mysqli_query($link, $query)){
        die("Error!");
}
unset($query); unset($row); unset($result);

$query = 'DROP TABLE website_' . $moduleNumber . '_pages';
if(!$result = mysqli_query($link, $query)){
        die("Error!");
}
unset($query); unset($row); unset($result);
?>
<h1>Deleted <?php echo $moduleName; ?></h1>
<?php

} //end if a module number is set
} //end check if user is an admin
?>
