<?php
////////////////////////
//
//  admin_postEdit.php
//  Included by module.php
//  Admin Only
//  Edits posts for the
//  website module.
////////////////////////


if(((isset($_SESSION['website_' . $moduleNumber . '_admin_1'])) && (isset($_SESSION['website_' . $moduleNumber . '_admin_2'])) && ($_SESSION['website_' . $moduleNumber . '_admin_1']==$userRank) && ($_SESSION['website_' . $moduleNumber . '_admin_2']==$userRank))  && (($userRank==2) || ($userRank==3))){  //if you are an admin of the website


if((isset($_GET['p'])) && (is_numeric($_GET['p']))){
	$postNumber = $_GET['p'];
}else{
	if((isset($_POST['postNumber'])) && (is_numeric($_POST['postNumber']))){
		$postNumber = $_POST['postNumber'];
	}else{
		die("Error!");
	}
}

$link = db_connect($database_url, $database_username, $database_password, $database_name);
$updated =false;

if((isset($_POST['postName'])) && (isset($_POST['postContent'])) && (isset($_POST['postDate'])) && (isset($_POST['postAuthor']))){

	$postName = $_POST['postName'];
	$postContent = $_POST['postContent'];
	$postDate = $_POST['postDate'];
	$postAuthor = $_POST['postAuthor'];
	if(isset($_POST['postComments'])){ $postComments=1; }else{ $postComments=0; }

	$query = 'UPDATE website_' . $moduleNumber . '_posts SET name=?,content=?,date=?,author=?,comments=? WHERE number=' . $postNumber;
        $stmt = mysqli_stmt_init($link);
        if(mysqli_stmt_prepare($stmt, $query)){
                mysqli_stmt_bind_param($stmt, "ssssi", $postName, $postContent, $postDate, $postAuthor, $postComments);
                mysqli_stmt_execute($stmt);
                mysqli_stmt_close($stmt);
                unset($query);
        }else{ //if stmt_prepare fails:
                die("Error!");
        }

	if(isset($_POST['removePages'])){
	$removePages = $_POST['removePages'];
	foreach($removePages as $page){
                $query = 'DELETE FROM website_' . $moduleNumber . '_plink WHERE pageNumber=' . $page . ' AND postNumber=' . $postNumber;
                if(!$result = mysqli_query($link, $query)){
                        die("Error!");
                }
                unset($query); unset($result);
        }
	}


	if(isset($_POST['addPages'])){
	$addPages = $_POST['addPages'];
	foreach($addPages as $pages){
                $query = 'INSERT INTO website_' . $moduleNumber . '_plink (pageNumber,postNumber) VALUES(' . $pages . ',' . $postNumber . ')';
                if(!$result = mysqli_query($link, $query)){
                        die("Error!");
                }
                unset($query); unset($result);
        }
	}

	$updated = true;
}

//Get post info:
$query = 'SELECT name,content,date,author,comments FROM website_' . $moduleNumber . '_posts WHERE number=' . $postNumber;
if($result = mysqli_query($link, $query)){
	while($row = mysqli_fetch_object($result)){
                $postName = $row->name;
		$postContent = $row->content;
		$postDate = $row->date;
		$postAuthor = $row->author;
		$postComments = $row->comments;
        }
}else{
	die("Error!<br>" . $query);
}
unset($query); unset($row); unset($result);

include('.' . $modulePath . 'admin_header.php');
?>

<h1>Edit (<?php echo $postNumber . ") " . $postName; ?></h1>

<?php
if($updated){ //if updated
?>
<h3>Updated!</h3>
<?php
} //end if updated
?>
<form action="./index.php?m=<?php echo $moduleNumber; ?>&a=4" method="POST">
<input type="hidden" name="postNumber" value="<?php echo $postNumber; ?>">
<label>Name: <input type="text" name="postName" value="<?php echo $postName; ?>"></label><br><br>
<label>Author: <input type="text" name="postAuthor" value="<?php echo $postAuthor; ?>"></label><br><br>
<label>Date: <input type="text" name="postDate" value="<?php echo $postDate; ?>"></label><br><br>
<label>Check to allow comments: <input type="checkbox" name="postComments" <?php if($postComments==1){ ?>checked<?php } ?>></label><br><br>

<div style="width: 200px; height: 100px; margin: auto; overflow-y: scroll; border: solid 2px black; text-align: left;">
Add to Page:<br>
<?php

        //Get all pages:
        $query = 'SELECT number,name FROM website_' . $moduleNumber . '_pages';
        $query = mysqli_real_escape_string($link, $query);
        if($result = mysqli_query($link, $query)){
                while($row = mysqli_fetch_object($result)){

			$found = false;

			$subQuery = 'SELECT postNumber FROM website_' . $moduleNumber . '_plink WHERE postNumber=' . $postNumber . ' AND pageNumber=' . $row->number;
		        $subQuery = mysqli_real_escape_string($link, $subQuery);
		        if($subResult = mysqli_query($link, $subQuery)){
		                while($subRow = mysqli_fetch_object($subResult)){
					$found = true;
				} //end sub while
			} //end sub if

			if($found==false){
?>
<label>&nbsp;&nbsp;<input type="checkbox" name="addPages[]" value="<?php echo $row->number; ?>"> <?php echo $row->name; ?></label><br>
<?php
			} //end if found==false
			unset($subQuery); unset($subResult); unset($subRow);
                } //end while
        } //end if
        unset($query); unset($row); unset($result);
?>
</div>

<br><br>

<div style="width: 200px; height: 100px; margin: auto; overflow-y: scroll; border: solid 2px black; text-align: left;">
Remove from Page:<br>
<?php

        //Get all pages:
        $query = 'SELECT pageNumber FROM website_' . $moduleNumber . '_plink WHERE postNumber=' . $postNumber;
        $query = mysqli_real_escape_string($link, $query);
        if($result = mysqli_query($link, $query)){
                while($row = mysqli_fetch_object($result)){

			$subQuery = 'SELECT name FROM website_' . $moduleNumber . '_pages WHERE number=' . $row->pageNumber;
                        $subQuery = mysqli_real_escape_string($link, $subQuery);
                        if($subResult = mysqli_query($link, $subQuery)){
                                while($subRow = mysqli_fetch_object($subResult)){
?>
<label>&nbsp;&nbsp;<input type="checkbox" name="removePages[]" value="<?php echo $row->pageNumber; ?>"> <?php echo $subRow->name; ?></label><br>
<?php
				} //end sub while
			} //end sub if
                } //end while
        } //end if
        unset($query); unset($row); unset($result);
        mysqli_close($link);
?>
</div>


<label>Content:<br>
<textarea rows="15" cols="50" name="postContent"><?php echo $postContent; ?></textarea></label><br><br><br>
<input type="submit" value="Edit">
</form>


<br><br>
<a href="./index.php?m=<?php echo $moduleNumber; ?>&post=<?php echo $postNumber; ?>">See Post ></a>
<br><br>

<?php
include('.' . $modulePath . 'admin_footer.php');
} //if you are an admin of the website
?>
