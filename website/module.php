<?php
//////////////////////////////////
//
//  module.php
//  Loaded from index.php on root.
//  Determines what the user wants
//    based on GET data and includes
//    the appropriate files.
//////////////////////////////////

require_once('installation_variables.php');

//Get the $themePath for this installation (below):
$link = db_connect($database_url, $database_username, $database_password, $database_name);
$query = 'SELECT textValue FROM website_' . $moduleNumber . '_settings WHERE name="theme"';

if($resultLink = mysqli_query($link, $query)){
$result = mysqli_fetch_assoc($resultLink);
$themePath='themes/' . $result['textValue'] . '/';
}

mysqli_free_result($resultLink);
unset($query); unset($result);
//Get the $themePath for this installation (above)

//Get links from website_(module#)_settings below:
$query = 'SELECT textValue FROM website_' . $moduleNumber . '_settings WHERE name="links"';

if($resultLink = mysqli_query($link, $query)){
$result = mysqli_fetch_assoc($resultLink);
$pageLinks = $result['textValue'];

mysqli_free_result($resultLink);
unset($result); unset($resultLink);
}else{
die("Error!");
}
unset($query);
mysqli_close($link);
//Get links from website_(module#)_settings above

if((isset($_GET['page'])) && (is_numeric(trim($_GET['page'])))){ //if we are loading a page
	//load page
	$pageNumber = trim($_GET['page']);
	include('page.php');
}else{ //if we aren't loading a page
	if((isset($_GET['e'])) && (is_numeric(trim($_GET['e'])))){ //if we are loading an extra page (join/login/etc...)
		$extraNumber=trim($_GET['e']);
		switch($extraNumber){
		case 1: require_once('join.php'); break; //Join
		case 2: require_once('login.php'); break; //Login
		case 3: require_once('logout.php'); break; //Logout
		case 4: require_once('activateAccount.php'); break; //Activate Accunt
		case 5: require_once('ban.php'); break; //Ban User
		case 6: require_once('delete.php'); break; //Delete a Comment
		default: require_once('login.php');
		}
	}else{  //if we aren't loading an extra page
		if((isset($_GET['post'])) && (is_numeric(trim($_GET['post'])))){ //if we are loading a post
			//load post
			$postNumber=trim($_GET['post']);
			include('post.php');
		}else{ //if we aren't loading a post
			if((isset($_GET['a'])) && (is_numeric(trim($_GET['a']))) && (($userRank==2) || ($userRank==3))){
				//if we are going to the admin panel

				//Double check userRank!
				$link = db_connect($database_url, $database_username, $database_password, $database_name);
			        $query = 'SELECT rank FROM website_' . $moduleNumber . '_users WHERE number=' . $_SESSION['user_number'];
			        if($resultLink = mysqli_query($link, $query)){
			                $result = mysqli_fetch_assoc($resultLink);
			                $userRank=$result['rank']; //Should NOT be a session variable; We don't want this to follow users around and cause an error!
					$_SESSION['website_' . $moduleNumber . '_admin_1'] = $userRank;
			        }
			        mysqli_free_result($resultLink);
			        mysqli_close($link);
			        unset($query); unset($result); unset($link);
				//Double check userRank!

				if((isset($_SESSION['website_' . $moduleNumber . '_admin_2'])) && ($_SESSION['website_' . $moduleNumber . '_admin_1']>2) && ($_SESSION['website_' . $moduleNumber . '_admin_2']==$userRank)){
					//if user has gone through the admin login
					switch(trim($_GET['a'])){
						case 1: include('.' . $modulePath . 'admin_settings.php'); break;
						case 2: include('.' . $modulePath . 'admin_userList.php'); break;
						case 3: include('.' . $modulePath . 'admin_postList.php'); break;
						case 4: include('.' . $modulePath . 'admin_postEdit.php'); break;
						case 5: include('.' . $modulePath . 'admin_postCreate.php'); break;
						case 6: include('.' . $modulePath . 'admin_pageList.php'); break;
						case 7: include('.' . $modulePath . 'admin_pageEdit.php'); break;
						case 8: include('.' . $modulePath . 'admin_pageCreate.php'); break;
						case 9: include('.' . $modulePath . 'admin_links.php'); break;
						case 10: include('.' . $modulePath . 'admin_postDelete.php'); break;
						case 11: include('.' . $modulePath . 'admin_pageDelete.php'); break;
						case 12: include('.' . $modulePath . 'admin_userEditor.php'); break;
						case 13: include('.' . $modulePath . 'admin_theme.php'); break;
						default: include('.' . $modulePath . 'admin_logout.php'); break;
					}
				}else{
					include('.' . $modulePath . 'admin_login.php');
				}
			}else{
				//if we aren't going to the admin panel
				//Get homePage from website_(module#)_settings below:
				$link = db_connect($database_url, $database_username, $database_password, $database_name);
				$query = 'SELECT intValue FROM website_' . $moduleNumber . '_settings WHERE name="homePage"';

				if(!$link){ die("Error!"); } //this needs to change for language packs in future versions
				if($result = mysqli_query($link, $query)){
					$row = mysqli_fetch_assoc($result);
					$pageNumber = $row['intValue'];
				}else{
					die("Error!");
				}
				mysqli_free_result($result); unset($query); unset($result); unset($row);
				//Get homePage from website_(module#)_settings above

				include('page.php');
			} //end if we aren't going to the admin panel
		}//end if we aren't loading a post
	}//end if we aren't loading an extra page
}//end if we aren't loading a page
?>
