<?php
////////////////////////////////////
//
// admin_logout.php
// Included by module.php
// Removes admin session variables
//   and links user back to module.
///////////////////////////////////

if((isset($admin_load_check)) || (isset($_SESSION['is_admin_1'])) || (isset($_SESSION['is_admin_2'])) || (isset($_SESSION['website_' . $moduleNumber . '_admin_1'])) || (isset($_SESSION['website_' . $moduleNumber . '_admin_2']))){

unset($admin_load_check);
unset($_SESSION['is_admin_1']);
unset($_SESSION['is_admin_2']);
unset($_SESSION['website_' . $moduleNumber . '_admin_1']);
unset($_SESSION['website_' . $moduleNumber . '_admin_2']);
?>

<html>
<head>
<title>Admin Logout</title>
</head>
<body>
<br><br><br>
<div style="margin: auto; text-align: center; border: solid 1px black;">
You have logged out!<br><a href="./index.php?m=<?php echo $moduleNumber; ?>">Continue</a>
</div>
</body>
</html>


<?php
}else{
die("Error!");
}
?>
