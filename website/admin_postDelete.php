<?php
////////////////////////
//
//  admin_postDelete.php
//  Included by module.php
//  Admins only
//  Deletes a post.
////////////////////////


if(((isset($_SESSION['website_' . $moduleNumber . '_admin_1'])) && (isset($_SESSION['website_' . $moduleNumber . '_admin_2'])) && ($_SESSION['website_' . $moduleNumber . '_admin_1']==$userRank) && ($_SESSION['website_' . $moduleNumber . '_admin_2']==$userRank))  && (($userRank==2) || ($userRank==3))){  //if you are an admin of the website

$link = db_connect($database_url, $database_username, $database_password, $database_name);  //keep this open!

if((isset($_GET['p'])) && (is_numeric($_GET['p']))){ $postNumber = $_GET['p']; }else{ session_destroy(); die("Error!"); }

//Query this post to get name to double check that user clicked on the right thing
$query = 'SELECT name,date,author FROM website_' . $moduleNumber . '_posts WHERE number=' . $postNumber;
if($result = mysqli_query($link, $query)){
        while($row = mysqli_fetch_object($result)){
                $postName = $row->name;
		$postDate = $row->date;
		$postAuthor = $row->author;
        }
}
unset($query); unset($row); unset($result);


if((isset($_GET['v'])) && ($_GET['v']==1)){
	//if we have confirmed to delete the page

	//delete plinks
        $query = 'DELETE FROM website_' . $moduleNumber . '_plink WHERE postNumber=' . $postNumber;
        if(!$result = mysqli_query($link, $query)){
                die("Error!");
        }
        unset($query); unset($result);

	//delete comments
        $query = 'DELETE FROM website_' . $moduleNumber . '_comments WHERE postNumber=' . $postNumber;
        if(!$result = mysqli_query($link, $query)){
                die("Error!");
        }
        unset($query); unset($result);

	//delete the page
	$query = 'DELETE FROM website_' . $moduleNumber . '_posts WHERE number=' . $postNumber;
	if(!$result = mysqli_query($link, $query)){
        	die("Error!");
	}
	unset($query); unset($result);

include('.' . $modulePath . 'admin_header.php');
?>
<h1>Deleted <?php echo $postName; ?>!</h1>
<?php
include('.' . $modulePath . 'admin_footer.php');
}

mysqli_close($link);

if((!isset($_GET['v'])) || ($_GET['v']!=1)){ //if not confirmed
include('.' . $modulePath . 'admin_header.php');
?>
<h1>Are you sure you want to delete the post <?php echo "(" . $postNumber . ") " . $postName; ?> create by <?php echo $postAuthor; ?> on <?php echo $postDate; ?>?</h1><h2>You cannot undo this!</h2>
<a href="./index.php?m=<?php echo $moduleNumber; ?>&a=10&p=<?php echo $postNumber; ?>&v=1">Yes, Continue.</a>
<?php
include('.' . $modulePath . 'admin_footer.php');
} //end if not confirmed
} //if you are an admin of the website
?>
