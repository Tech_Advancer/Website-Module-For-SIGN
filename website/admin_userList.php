<?php
////////////////////////
//
//  admin_userList.php
//  Included by module.php
//  Admin Only
//  Lists all users on sign
//  so admins can premote/
//  demote users.
////////////////////////


if(((isset($_SESSION['website_' . $moduleNumber . '_admin_1'])) && (isset($_SESSION['website_' . $moduleNumber . '_admin_2'])) && ($_SESSION['website_' . $moduleNumber . '_admin_1']==$userRank) && ($_SESSION['website_' . $moduleNumber . '_admin_2']==$userRank))  && (($userRank==2) || ($userRank==3))){  //if you are an admin of the website

include('.' . $modulePath . 'admin_header.php');
?>

<h1>User List</h1>

<table style="width: 95%; margin: auto; border-collapse: collapse;">
<?php
$link = db_connect($database_url, $database_username, $database_password, $database_name);

//Get all of the posts:
$query = 'SELECT number,username FROM shared_users';
$query = mysqli_real_escape_string($link, $query);

if($result = mysqli_query($link, $query)){
	while($row = mysqli_fetch_object($result)){
?>
<tr><td>
<a href="./index.php?m=<?php echo $moduleNumber; ?>&p=<?php echo $row->number; ?>&a=12">(<?php echo $row->number . ") &nbsp;" . $row->username; ?></a>
</td></tr>
<?php
	}
}
unset($query); unset($row); unset($result);
mysqli_close($link);
?>
</table>
<?php
include('.' . $modulePath . 'admin_footer.php');
} //if you are an admin of the website
?>
