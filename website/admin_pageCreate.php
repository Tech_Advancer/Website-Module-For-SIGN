<?php
////////////////////////
//
//  admin_pageCreate.php
//  Included by module.php
//  Makes pages for the
//  website module.
////////////////////////


if(((isset($_SESSION['website_' . $moduleNumber . '_admin_1'])) && (isset($_SESSION['website_' . $moduleNumber . '_admin_2'])) && ($_SESSION['website_' . $moduleNumber . '_admin_1']==$userRank) && ($_SESSION['website_' . $moduleNumber . '_admin_2']==$userRank))  && (($userRank==2) || ($userRank==3))){  //if you are an admin of the website


if((isset($_POST['pageName'])) && (isset($_POST['pageType']))){
//if form was submitted

	$link = db_connect($database_url, $database_username, $database_password, $database_name);  //keep this open!

        $pageName = db_safe($_POST['pageName'], $link);
        $pageType = db_safe($_POST['pageType'], $link);
        $pageContent = db_safe($_POST['pageContent'], $link);

        $query = 'INSERT INTO website_' . $moduleNumber . '_pages (name,content,type,date,author) VALUES (?,?,?,?,?)';
        $stmt = mysqli_stmt_init($link);
        if(mysqli_stmt_prepare($stmt, $query)){
		$tempDate = date("Y-m-d");
                mysqli_stmt_bind_param($stmt, "ssiss", $pageName, $pageContent, $pageType, $tempDate, $_SESSION['user_name']);
                mysqli_stmt_execute($stmt);
                $pageNumber = mysqli_insert_id($link);
                mysqli_stmt_close($stmt);
                unset($query);
        }else{ //if stmt_prepare fails:
                die("Error!");
        }
	mysqli_close($link);

	include('.' . $modulePath . 'admin_header.php');
?>
<h1>Page Created!</h1>
<p><a href="./index.php?m=<?php echo $moduleNumber; ?>&a=7&p=<?php echo $pageNumber; ?>">Click Here to edit</a> or
<a href="./index.php?m=<?php echo $moduleNumber; ?>&page=<?php echo $pageNumber; ?>">click here to view</a> your new page!</p>
<?php
	include('.' . $modulePath . 'admin_footer.php');


}else{
//if form hasn't been submitted
	include('.' . $modulePath . 'admin_header.php');
?>

<h1>Create a New Page for <?php echo $moduleName; ?></h1>

<form action="./index.php?m=<?php echo $moduleNumber; ?>&a=8" method="POST">
<label>Page Name: <input type="text" name="pageName"></label><br>
<label>Page Type:
<select name="pageType">
<option value="1">Normal Page</option>
<option value="2">List Page</option>
<option value="3">Static Page</option>
<option value="4">Blank Static Page</option>
</select></label><br><br>
<label>Top Post/Page Content:<br>
<textarea name="pageContent"></textarea></label><br><br>
<input type="submit" value="Create">
</form>

<?php
	include('.' . $modulePath . 'admin_footer.php');
} // end if the form has been submitted
} //if you are an admin of the website

?>
